package main

import (
	"net/http"
)

func main() {

	go func() {
		if err := http.ListenAndServe(":8080", http.FileServer(http.Dir('.'))); err != nil {
			panic(err)
		}
	}()

	http.ListenAndServe(":8090", http.FileServer(http.Dir('.')))
}
